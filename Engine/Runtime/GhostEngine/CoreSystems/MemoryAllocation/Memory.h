#pragma once

#include "GhostEngine.h"

class CMemory
{
public:
	INLINE static void* Malloc(size_t size)
	{
		return malloc(size);
	}

	INLINE static void* Calloc(size_t count, size_t size)
	{
		return calloc(count, size);
	}

	INLINE static void* Realloc(void* block, size_t size)
	{
		return realloc(block, size);
	}

	INLINE static void Free(void* block)
	{
		return free(block);
	}
};