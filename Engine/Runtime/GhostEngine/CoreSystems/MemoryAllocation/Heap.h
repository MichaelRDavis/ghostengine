#pragma once

#include "GhostEngine.h"

enum EMemoryTag
{
	MEMTAG_None,
	MEMTAG_New,
	MEMTAG_NewArray,
	MEMTAG_DynArray,
	MEMTAG_Max
};

static const char* MemTags[MEMTAG_Max] =
{
	"None		",
	"New		",
	"NewArray	",
	"DynArray	",
};

struct SMemoryStats
{
	static uint64 numAllocated;
	static uint64 numTagAllocated[MEMTAG_Max];
};

void InitHeap();
void DestroyHeap();

GHOSTENGINE_API void* Allocate(size_t size, EMemoryTag tag);
GHOSTENGINE_API void Free(void* block, size_t size, EMemoryTag tag);

char* GetMemoryUsage();

#pragma warning(push)
#pragma warning(disable : 4595)
INLINE void* operator new(size_t size)
{
	return Allocate(size, MEMTAG_New);
}

INLINE void operator delete(void* ptr)
{
	Free(ptr, sizeof(ptr), MEMTAG_New);
}

INLINE void* operator new[](size_t size)
{
	return Allocate(size, MEMTAG_NewArray);
}

INLINE void operator delete[](void* ptr)
{
	Free(ptr, sizeof(ptr), MEMTAG_NewArray);
}

INLINE void* operator new(size_t size, EMemoryTag tag)
{
	return Allocate(size, tag);
}

INLINE void operator delete(void* ptr, EMemoryTag tag)
{
	Free(ptr, sizeof(ptr), tag);
}

INLINE void* operator new[](size_t size, EMemoryTag tag)
{
	return Allocate(size, tag);
}

INLINE void operator delete[](void* ptr, EMemoryTag tag)
{
	Free(ptr, sizeof(ptr), tag);
}
#pragma warning(pop) 