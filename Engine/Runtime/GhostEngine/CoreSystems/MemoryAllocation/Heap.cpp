#include "Heap.h"
#include "Memory.h"

uint64 SMemoryStats::numAllocated = 0;
uint64 SMemoryStats::numTagAllocated[MEMTAG_Max];

void InitHeap()
{

}

void DestroyHeap()
{

}

void* Allocate(size_t size, EMemoryTag tag)
{
	if (tag == MEMTAG_None)
	{
		GHOST_LOG(Warning, "No memory tag specified!");
	}

	SMemoryStats::numAllocated += size;
	SMemoryStats::numTagAllocated[tag] += size;

	void* block = CMemory::Malloc(size);
	return block;
}

void Free(void* block, size_t size, EMemoryTag tag)
{
	if (tag == MEMTAG_None)
	{
		GHOST_LOG(Warning, "No memory tag specified!");
	}

	SMemoryStats::numAllocated -= size;
	SMemoryStats::numTagAllocated[tag] -= size;

	CMemory::Free(block);
}

char* GetMemoryUsage()
{
	const uint64 gib = 1024 * 1024 * 1024;
	const uint64 mib = 1024 * 1024;
	const uint64 kib = 1024;

	char buffer[8000] = "System memory used (Tagged): \n";
	uint64 offset = strlen(buffer);
	for (uint32 i = 0; i < MEMTAG_Max; ++i)
	{
		char unit[4] = "XiB";
		float amount = 1.0f;
		if (SMemoryStats::numTagAllocated[i] >= gib)
		{
			unit[0] = 'G';
			amount = SMemoryStats::numTagAllocated[i] / (float)gib;
		}
		else if (SMemoryStats::numTagAllocated[i] >= mib)
		{
			unit[0] = 'M';
			amount = SMemoryStats::numTagAllocated[i] / (float)mib;
		}
		else if (SMemoryStats::numTagAllocated[i] >= kib)
		{
			unit[0] = 'K';
			amount = SMemoryStats::numTagAllocated[i] / (float)kib;
		}
		else
		{
			unit[0] = 'B';
			unit[1] = 0;
			amount = (float)SMemoryStats::numTagAllocated[i];
		}

		int32 length = snprintf(buffer + offset, 8000, " %s: %.2f%s\n", MemTags[i], amount, unit);
		offset += length;
	}

	char* outputBuffer = _strdup(buffer);
	return outputBuffer;
}