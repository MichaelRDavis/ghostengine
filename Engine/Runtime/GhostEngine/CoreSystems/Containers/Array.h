#pragma once

template<typename type, size_t size>
class TArray
{
public:
	TArray()
	{
		mElements = 0;
	}

	~TArray()
	{
		Clear();
	}

	FORCEINLINE const type& operator[](size_t index) const
	{
		GHOST_ASSERT(index >= 0);
		GHOST_ASSERT(index < mElements);
		return mArray[index];
	}

	FORCEINLINE type& operator[](size_t index)
	{
		GHOST_ASSERT(index >= 0);
		GHOST_ASSERT(index < mElements);
		return mArray[index];
	}

	FORCEINLINE const type* Front() const
	{
		return &mArray[0];
	}

	FORCEINLINE type* Front()
	{
		return &mArray[0];
	}

	FORCEINLINE const type* Back() const
	{
		return &mArray[size - 1];
	}

	FORCEINLINE type* Back()
	{
		return &mArray[size - 1];
	}

	FORCEINLINE const type* Data() const
	{
		return &mArray;
	}

	FORCEINLINE type* Data()
	{
		return &mArray;
	}

	FORCEINLINE size_t FindIndex(const type& value) const
	{
		for (size_t i = 0; i < mElements; i++)
		{
			if (mArray[i] == value)
			{
				return i;
			}
		}

		return -1;
	}

	FORCEINLINE type* Find(const type& value) const
	{
		size_t index = FindIndex(value);
		if (index >= 0)
		{
			return (type*)&mArray[i];
		}

		return nullptr;
	}

	FORCEINLINE void Add(const type& value)
	{
		Append(value);
	}

	FORCEINLINE size_t Append(const type& value)
	{
		GHOST_ASSERT(mElements < size);
		if (mElements < size)
		{
			mArray[mElements] = value;
			mElements++;
			return mElements - 1;
		}

		return -1;
	}

	FORCEINLINE void AddUnique(const type& value)
	{
		size_t index = FindIndex(value);
		if (index < 0)
		{
			Add(value);
		}
	}

	FORCEINLINE void Clear()
	{
		mElements = 0;
	}

	FORCEINLINE void Erase()
	{
		for (size_t i = 0; i < mElements; i++)
		{
			delete mArray[i];
			mArray[i] = nullptr;
		}
	}

	FORCEINLINE size_t Num() const
	{
		return mElements;
	}

	FORCEINLINE size_t Size() const
	{
		return size;
	}

	FORCEINLINE bool IsEmpty() const
	{
		return mElements == 0;
	}
	
private:
	type mArray[size];
	size_t mElements;
};