#pragma once

#include <new>

#include "CoreSystems/MemoryAllocation/Heap.h"

template<typename type>
class TDynArray
{
public:
	TDynArray()
	{
		mArray = nullptr;
		mElements = 0;
		mSize = 0;
		mStride = 0;
	}

	TDynArray(const TDynArray& other)
	{
		mArray = nullptr;
		*this = other;
	}

	~TDynArray()
	{
		Clear();
	}

	const type& operator[](size_t index) const
	{
		GHOST_ASSERT(index >= 0);
		GHOST_ASSERT(index < mElements);
		return mArray[index];
	}

	type& operator[](size_t index)
	{
		GHOST_ASSERT(index >= 0);
		GHOST_ASSERT(index < mElements);
		return mArray[index];
	}

	FORCEINLINE const type* Front() const
	{
		return &mArray[0];
	}

	FORCEINLINE type* Front()
	{
		return &mArray[0];
	}

	FORCEINLINE const type* Back() const
	{
		return &mArray[size - 1];
	}

	FORCEINLINE type* Back()
	{
		return &mArray[size - 1];
	}

	FORCEINLINE const type* Data() const
	{
		return mArray;
	}

	FORCEINLINE type* Data()
	{
		return mArray;
	}

	FORCEINLINE size_t Append(const type& value)
	{
		if (!mArray)
		{
			Resize(mGrowthFactor);
		}

		if (mElements == mSize)
		{
			size_t newSize = mSize + mGrowthFactor;
			Resize(newSize - newSize % mGrowthFactor);
		}

		mArray[mElements] = value;
		mElements++;

		return mElements - 1;
	}

	FORCEINLINE void Add(const type& value)
	{
		Append(value);
	}

	FORCEINLINE void AddUnique(const type& value)
	{
		size_t index = FindIndex(value);
		if (index < 0)
		{
			Append(value);
		}
	}

	FORCEINLINE size_t FindIndex(const type& value) const
	{
		if (!mArray)
		{
			return -1;
		}

		for (size_t i = 0; i < mElements; i++)
		{
			if (mArray[i] == value)
			{
				return i;
			}
		}

		return -1;
	}

	FORCEINLINE type* Find(const type& value) const
	{
		if (!mArray)
		{
			return nullptr;
		}

		size_t index;
		index = FindIndex(value);
		if (index >= 0)
		{
			return &mArray[index];
		}

		return nullptr;
	}

	FORCEINLINE void Clear()
	{
		if (mArray != nullptr)
		{
			FreeArray(mArray, mSize);
		}

		mArray = nullptr;
		mElements = 0;
		mSize = 0;
	}

	FORCEINLINE void Erase()
	{
		if (mArray != nullptr)
		{
			for (size_t i = 0; i < mElements; i++)
			{
				delete mArray[i];
				mArray[i] = nullptr;
			}
		}
	}

	FORCEINLINE void Resize(size_t newSize)
	{
		GHOST_ASSERT(newSize >= 0);

		if (newSize <= 0)
		{
			Clear();
			return;
		}

		mArray = (type*)ReallocateArray(mArray, mSize, newSize);
		mSize = newSize;
		if (mSize < mElements)
		{
			mElements = mSize;
		}
	}

	FORCEINLINE void Shrink()
	{
		if (mArray != nullptr)
		{
			if (mElements > 0)
			{
				Resize(mElements);
			}
			else
			{
				Clear();
			}
		}
	}

	FORCEINLINE size_t Num() const
	{
		return mElements;
	}

	FORCEINLINE size_t Size() const
	{
		return mSize;
	}

	FORCEINLINE bool IsEmpty() const
	{
		return mElements == 0;
	}

private:
	FORCEINLINE void* AllocateArray(size_t size)
	{
		type* newArray = nullptr;
		newArray = (type*)Allocate(sizeof(type) * size, MEMTAG_DynArray);
		for (size_t i = 0; i < size; i++)
		{
			new(&newArray[i])type();
		}

		return newArray;
	}

	FORCEINLINE void* ReallocateArray(void* inArray, size_t size, size_t newSize)
	{
		type* oldArray = (type*)inArray;
		type* newArray = nullptr;
		if (newSize > 0)
		{
			newArray = (type*)AllocateArray(newSize);
			size_t overlap = (size < newSize) ? size : newSize;
			for (size_t i = 0; i < overlap; i++)
			{
				newArray[i] = oldArray[i];
			}
		}

		FreeArray(inArray, size);
		return newArray;
	}

	FORCEINLINE void FreeArray(void* inArray, size_t size)
	{
		for (size_t i = 0; i < size; i++)
		{
			((type*)mArray)[i].~type();
		}

		Free(inArray, size, MEMTAG_DynArray);
	}
	
private:
	type* mArray;
	size_t mElements;
	size_t mSize;
	size_t mStride;
	size_t mGrowthFactor = 2;
};