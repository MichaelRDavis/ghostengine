#pragma once

#include "GhostEngine.h"

enum ELogVerbosity
{
	Fatal,
	Error,
	Warning,
	Info,
	Log,
	Display
};

struct SLogEntry
{
	const char* message;
	ELogVerbosity logVerbosity;
	uint32 category;
	time_t localtime;
};

GHOSTENGINE_API void LogMessage(ELogVerbosity verbosity, const char* message, ...);

#define GHOST_LOG(verbosity, message, ...) LogMessage(verbosity, message, ##__VA_ARGS__)