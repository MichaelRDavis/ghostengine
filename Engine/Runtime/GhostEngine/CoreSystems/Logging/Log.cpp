#include "Log.h"
#include "PlatformLayer/Platform/PlatformInterface/Platform.h"

void LogMessage(ELogVerbosity verbosity, const char* message, ...)
{
	const char* verbosities[6] =
	{
		"Fatal: ",
		"Error: ",
		"Warning: ",
		"Info: ",
		"Log: ",
		"Display: "
	};

	char messageBuffer[32000];
	memset(messageBuffer, 0, sizeof(messageBuffer));

#ifdef _MSC_VER
	va_list argPtr;
#elif defined(__clang__)
	__builtin_va_list argPtr;
#endif
	va_start(argPtr, message);
	vsnprintf(messageBuffer, 32000, message, argPtr);
	va_end(argPtr);

	char outputBuffer[32000];
	sprintf_s(outputBuffer, "%s%s\n", verbosities[verbosity], messageBuffer);

	bool isError = verbosity < 2;
	if (isError)
	{
		IPlatform::WriteConsoleError(outputBuffer, verbosity);
	}
	else
	{
		IPlatform::WriteConsole(outputBuffer, verbosity);
	}
}