#include "Assertions.h"

void AssertionFailed(const char* file, int32 line, const char* expression)
{
	if (expression)
	{

	}
	else
	{
		GHOST_LOG(Error, "Assertion Failed: %s(%d): '%s'", file, line, expression);
#ifdef _MSC_VER
		if (IsDebuggerPresent())
		{
			DEBUG_BREAK();
		}
#endif
	}
}