#pragma once

#include "GhostEngine.h"

#ifdef _MSC_VER
	#include <intrin.h>
	#define DEBUG_BREAK() __debugbreak()
#elif defined(__clang__)
	#define DEBUG_BREAK() __builtin_trap()
#endif

GHOSTENGINE_API void AssertionFailed(const char* file, int32 line, const char* expression);

#ifdef GHOST_DEBUG
	#define GHOST_ASSERT(expression) AssertionFailed(__FILE__, __LINE__, #expression)
#else
	#define GHOST_ASSERT()
#endif