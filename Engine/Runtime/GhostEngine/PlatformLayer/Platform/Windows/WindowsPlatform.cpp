#include "WindowsPlatform.h"

HINSTANCE CWindowsPlatform::mInstance = nullptr;
HWND CWindowsPlatform::mWindowHandle = nullptr;
LPCWSTR CWindowsPlatform::mAppWinClass = L"GhostApplication";
float CWindowsPlatform::mClockFrequency = 0.0f;
LARGE_INTEGER CWindowsPlatform::mStartTime;

void CWindowsPlatform::CreateConsole()
{
	AllocConsole();
}

void CWindowsPlatform::DestroyConsole()
{
	FreeConsole();
}

void CWindowsPlatform::WriteConsole(const char* msg, uint8 color)
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (consoleHandle != nullptr)
	{
		static uint8 colors[6] = {64, 4, 6, 2, 1, 8};
		SetConsoleTextAttribute(consoleHandle, colors[color]);

		LPDWORD numWritten = 0;
		WriteConsoleA(consoleHandle, msg, (DWORD)strlen(msg), numWritten, 0);
	}

	OutputDebugStringA(msg);
}

void CWindowsPlatform::WriteConsoleError(const char* msg, uint8 color)
{
	HANDLE consoleHandle = GetStdHandle(STD_ERROR_HANDLE);
	if (consoleHandle != nullptr)
	{
		static uint8 colors[6] = { 64, 4, 6, 2, 1, 8 };
		SetConsoleTextAttribute(consoleHandle, colors[color]);

		LPDWORD numWritten = 0;
		WriteConsoleA(consoleHandle, msg, (DWORD)strlen(msg), numWritten, 0);
	}

	OutputDebugStringA(msg);
}

void CWindowsPlatform::CreateApplication(const SApplicationParams& params)
{
	mInstance = GetModuleHandle(0);

	WNDCLASS windowClass;
	memset(&windowClass, 0, sizeof(windowClass));
	windowClass.style = CS_DBLCLKS;
	windowClass.lpfnWndProc = CWindowsPlatform::WindowProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = mInstance;
	windowClass.hIcon = LoadIcon(mInstance, IDI_APPLICATION);
	windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClass.hbrBackground = nullptr;
	windowClass.lpszMenuName = nullptr;
	windowClass.lpszClassName = CWindowsPlatform::mAppWinClass;
	if (!RegisterClass(&windowClass))
	{
		MessageBoxA(nullptr, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	uint32 windowStyle = WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION;
	uint32 windowEXStyle = WS_EX_APPWINDOW;
	windowStyle |= WS_MAXIMIZEBOX;
	windowStyle |= WS_MAXIMIZEBOX;
	windowStyle |= WS_THICKFRAME;

	RECT borderRect = { 0, 0, 0, 0 };
	AdjustWindowRectEx(&borderRect, windowStyle, false, windowEXStyle);

	mWindowHandle = CreateWindow(
		CWindowsPlatform::mAppWinClass,
		CWindowsPlatform::mAppWinClass,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		params.width,
		params.height,
		nullptr,
		nullptr,
		mInstance,
		nullptr);
	if (mWindowHandle == nullptr)
	{
		MessageBoxA(nullptr, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	ShowWindow(mWindowHandle, SW_SHOW);
}

void CWindowsPlatform::DestroyApplication()
{
	if (mWindowHandle != nullptr)
	{
		DestroyWindow(mWindowHandle);
		mWindowHandle = nullptr;
	}

	FreeConsole();
}

void CWindowsPlatform::PumpMessages()
{
	MSG message;
	while (PeekMessageA(&message, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
}

void CWindowsPlatform::StartClock()
{
	LARGE_INTEGER frequency;
	QueryPerformanceCounter(&frequency);
	mClockFrequency = 1.0f / (float)frequency.QuadPart;
	QueryPerformanceCounter(&mStartTime);
}

float CWindowsPlatform::GetAbsoluteTime()
{
	LARGE_INTEGER currentTime;
	QueryPerformanceCounter(&currentTime);
	return (float)currentTime.QuadPart * mClockFrequency;
}

void CWindowsPlatform::PlatformSleep(uint64 milliseconds)
{
	Sleep((DWORD)milliseconds);
}

LRESULT CALLBACK CWindowsPlatform::WindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		DestroyApplication();
		return 0;
	}

	return DefWindowProc(hwnd, message, wparam, lparam);
}