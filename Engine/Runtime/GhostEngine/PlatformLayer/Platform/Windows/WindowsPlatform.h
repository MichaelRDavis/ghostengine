#pragma once

#include "GhostEngine.h"
#include "PlatformLayer/Platform/PlatformInterface/Platform.h"

#ifdef GHOST_PLATFORM_WINDOWS
class CWindowsPlatform : public IPlatform
{
public:
	static void CreateConsole();

	static void DestroyConsole();

	static void WriteConsole(const char* msg, uint8 color);

	static void WriteConsoleError(const char* msg, uint8 color);

	static void CreateApplication(const SApplicationParams& params);

	static void DestroyApplication();

	static void PumpMessages();

	static void StartClock();

	static float GetAbsoluteTime();

	static void PlatformSleep(uint64 milliseconds);

private:
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

private:
	static HINSTANCE mInstance;
	static HWND mWindowHandle;
	static LPCWSTR mAppWinClass;

	static float mClockFrequency;
	static LARGE_INTEGER mStartTime;
};
#endif GHOST_PLATFORM_WINDOWS