#pragma once

#include "GhostEngine.h"

#define MAX_EVENT_CODES 16834

enum EApplicationEventCodes
{
	EVENT_Quit = 0x01,
	EVENT_KeyPressed = 0x02,
	EVENT_KeyReleased = 0x03,
	EVENT_ButtonPressed = 0x04,
	EVENT_ButtonReleased = 0x05,
	EVENT_MouseMove = 0x06,
	EVENT_MouseWheel = 0x07,
	EVENT_Resized = 0x08,
	EVENT_Max = 0xFF
};

struct SEventContext
{
	union UData
	{
		int64 integer64[2];
		uint64 uinteger64[2];
		double float64[2];

		int32 integer32[4];
		uint32 uinteger[4];
		float float32[4];

		int16 integer16[8];
		uint16 uinteger16[8];

		int8 integer8[16];
		uint8 uineteger[16];

		char character[16];
	};
};

typedef bool (*On_Event)(uint16 code, void* sender, void* listener, SEventContext data);

struct SRegsiteredEvent
{
	void* listener;
	On_Event callback;
};

struct SEvents
{
	SRegsiteredEvent* events;
};

class CMessageHandler
{
	void InitMessageHandler();
	void DestroyMessageHandler();

	GHOSTENGINE_API bool RegisterEvent(uint16 code, void* listener, On_Event onEvent);
	GHOSTENGINE_API bool UnregisterEvent(uint16 code, void* listener, On_Event onEvent);
	GHOSTENGINE_API bool SendEvent(uint16 code, void* sender, SEventContext context);

private:
	SEvents registered[MAX_EVENT_CODES];
	bool mIsInitialized = false;
};