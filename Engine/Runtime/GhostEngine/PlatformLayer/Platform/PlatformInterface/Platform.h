#pragma once

#include "GhostEngine.h"

struct SApplicationParams
{
	const char* appTitle;
	int32 posX;
	int32 poxY;
	int32 width;
	int32 height;
};

class GHOSTENGINE_API IPlatform
{
public:
	static void CreateConsole();

	static void DestroyConsole();

	static void WriteConsole(const char* msg, uint8 color);

	static void WriteConsoleError(const char* msg, uint8 color);

	static void CreateApplication(const SApplicationParams& params);

	static void DestroyApplication();

	static void PumpMessages();

	static void StartClock();

	static float GetAbsoluteTime();

	static void PlatformSleep(uint64 milliseconds);
};