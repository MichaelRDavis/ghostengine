#include "Platform.h"
#include "PlatformLayer/Platform/Windows/WindowsPlatform.h"

void IPlatform::CreateConsole()
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::CreateConsole();
#endif
}

void IPlatform::DestroyConsole()
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::DestroyConsole();
#endif
}

void IPlatform::WriteConsole(const char* msg, uint8 color)
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::WriteConsoleError(msg, color);
#endif
}

void IPlatform::WriteConsoleError(const char* msg, uint8 color)
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::WriteConsoleError(msg, color);
#endif
}

void IPlatform::CreateApplication(const SApplicationParams& params)
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::CreateApplication(params);
#endif
}

void IPlatform::DestroyApplication()
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::DestroyApplication();
#endif
}

void IPlatform::PumpMessages()
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::PumpMessages();
#endif
}

void IPlatform::StartClock()
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::StartClock();
#endif
}

float IPlatform::GetAbsoluteTime()
{
#ifdef GHOST_PLATFORM_WINDOWS
	return CWindowsPlatform::GetAbsoluteTime();
#endif

	return 0.0f;
}

void IPlatform::PlatformSleep(uint64 milliseconds)
{
#ifdef GHOST_PLATFORM_WINDOWS
	CWindowsPlatform::PlatformSleep(milliseconds);
#endif
}