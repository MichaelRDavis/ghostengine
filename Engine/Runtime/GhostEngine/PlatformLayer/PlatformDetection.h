#pragma once

#ifdef _WIN32
	#ifdef _WIN64
		#define GHOST_PLATFORM_WINDOWS
	#else
		#error "Win32 builds are not supported!"
	#endif
#elif defined(__ANDROID__)
	#error "Android platform is not supported!"
#elif defined(__linux__)
	#define GHOST_PLATFORM_LINUX
#elif defined (__APPLE__) || defined(__MACH__)
	#if TARGET_IPHONE_SIMULATOR
		#error "IOS simulator platform not supported!"
	#elif TARGET_OS_PHONE
		#error "IOS platform not supported!"
	#elif TARGET_OS_MAC
		#define GHOST_PLATFORM_MACOS
	#else
		#error "Apple platform not supported!"
	#endif
#else
	#error "Unknown platform not supported!"
#endif