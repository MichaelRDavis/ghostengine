#pragma once

#ifdef _MSC_VER
	typedef __int8 int8;
	typedef unsigned __int8 uint8;
	typedef __int16 int16;
	typedef unsigned __int16 uint16;
	typedef __int32 int32;
	typedef unsigned __int32 uint32;
	typedef __int64 int64;
	typedef unsigned __int64 uint64;
#elif defined(__clang__)
	typedef signed char int8;
	typedef unsigned char uint8;
	typedef short int int16;
	typedef unsigned short int uint16;
	typedef int int32;
	typedef unsigned uint32;
	typedef long long int int64;
	typedef unsigned long long int uint64;
#endif

static_assert(sizeof(int8) == 1, "int8 not 1 byte!");
static_assert(sizeof(uint8) == 1, "uint8 not 1 byte!");
static_assert(sizeof(int16) == 2, "int16 not 2 bytes!");
static_assert(sizeof(uint16) == 2, "uint16 not 2 bytes!");
static_assert(sizeof(int32) == 4, "int32 not 4 bytes!");
static_assert(sizeof(uint32) == 4, "uint32 not 4 bytes!");
static_assert(sizeof(int64) == 8, "int64 not 8 bytes!");
static_assert(sizeof(uint64) == 8, "uint64 not 8 bytes!");