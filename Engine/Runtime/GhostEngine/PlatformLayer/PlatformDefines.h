#pragma once

#ifdef _MSC_VER
	#define INLINE __inline
	#define FORCEINLINE __forceinline
	#define DLLEXPORT __declspec(dllexport)
	#define DLLIMPORT __declspec(dllimport)
#elif defined(__clang__)
	#define INLINE inline
	#define FORCEINLINE [[clang::always_inline]]
	#define DLLEXPORT __attribute__((visibility(default)))
	#define DLLIMPORT 
#endif