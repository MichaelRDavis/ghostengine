#pragma once

#include "PlatformLayer/PlatformDetection.h"
#include "PlatformLayer/PlatformTypes.h"
#include "PlatformLayer/PlatformDefines.h"
#include "PlatformLayer/PlatformIncludes.h"

#ifdef GHOST_EXPORT
	#define GHOSTENGINE_API DLLEXPORT
#else
	#define GHOSTENGINE_API DLLIMPORT
#endif

#include "CoreSystems/Logging/Log.h"
#include "CoreSystems/Logging/Assertions.h"
#include "CoreSystems/MemoryAllocation/Memory.h"
//#include "CoreSystems/MemoryAllocation/Heap.h"
#include "CoreSystems/Containers/Array.h"
#include "CoreSystems/Containers/DynArray.h"