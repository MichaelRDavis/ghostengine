#include "Engine.h"
#include "PlatformLayer/Platform/PlatformInterface/Platform.h"

bool CEngine::mIsInitialized  = false;

CEngine::CEngine()
{
	mLastTime = 0.0f;
	mIsRunning = false;
	mIsSuspended = false;
}

CEngine::~CEngine()
{

}

void CEngine::AppInit(struct SApplicationParams& params)
{
	if (mIsInitialized)
	{
		GHOST_LOG(Error, "Engine initialized more than once.");
		return;
	}

	IPlatform::CreateConsole();
	IPlatform::CreateApplication(params);

	GHOST_LOG(Display, "%s", GetMemoryUsage());

	mIsRunning = true;
	mIsInitialized = true;
}

void CEngine::AppRun()
{
	while (mIsRunning)
	{
		IPlatform::PumpMessages();
	}

	mIsRunning = false;
	AppExit();
}

void CEngine::AppExit()
{
	IPlatform::DestroyApplication();
}