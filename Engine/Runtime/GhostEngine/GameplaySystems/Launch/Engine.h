#pragma once

#include "GhostEngine.h"

class GHOSTENGINE_API CEngine
{
public:
	CEngine();
	~CEngine();

	void AppInit(struct SApplicationParams& params);

	void AppRun();

	void AppExit();

private:
	float mLastTime;
	bool mIsRunning;
	bool mIsSuspended;
	static bool mIsInitialized;
};