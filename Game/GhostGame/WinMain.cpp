#include "GhostEngine.h"
#include "PlatformLayer/Platform/PlatformInterface/Platform.h"
#include "GameplaySystems/Launch/Engine.h"

#ifdef GHOST_PLATFORM_WINDOWS
int WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
	SApplicationParams params;
	params.appTitle = "GhostGame";
	params.width = 1920;
	params.height = 1080;

	CEngine engine;
	engine.AppInit(params);
	engine.AppRun();

	return 0;
}
#endif