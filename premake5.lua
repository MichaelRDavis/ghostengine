workspace "GhostEngine"
    startproject "GhostGame"

    configurations
    {
        "Debug",
        "Shipping"
    }

    platforms
    {
        "Windows",
    }

    filter "platforms:Windows"
        system "windows"
        architecture "x86_64"

    group "Engine"
        project "GhostEngine"
    group "Game"
        project "GhostGame"

project "GhostEngine"
    location "Engine/Runtime/GhostEngine"
    kind "SharedLib"
    language "C++"
    cppdialect "C++11"
    targetdir "Binaries"
     objdir "Intermediate"

    defines {"GHOST_EXPORT"}

    files
    {
        "Engine/Runtime/GhostEngine/**.h",
        "Engine/Runtime/GhostEngine/**.cpp"
    }

    includedirs
    {
        "Engine/Runtime/GhostEngine"
    }

    libdirs
    {
        "Binaries"
    }

    filter "configurations:Debug"
        defines {"GHOST_DEBUG"}
        symbols "On"
        optimize "Debug"

    filter "configurations:Shipping"
        defines {"GHOST_SHIPPING"}
        symbols "Off"
        optimize "Full"

project "GhostGame"
    location "Game/GhostGame"
    kind "WindowedApp"
    language "C++"
    cppdialect "C++11"
    targetdir "Binaries"
    objdir "Intermediate"

    files
    {
        "Game/GhostGame/**.h",
        "Game/GhostGame/**.cpp"
    }

    includedirs
    {
        "Engine/Runtime/GhostEngine",
        "Game/GhostGame"
    }

    libdirs
    {
        "Binaries"
    }

    links
    {
        "GhostEngine"
    }

filter "configurations:Debug"
    defines {"GHOST_DEBUG"}
    symbols "On"
    optimize "Debug"

filter "configurations:Shipping"
    defines {"GHOST_SHIPPING"}
    symbols "Off"
    optimize "Full"